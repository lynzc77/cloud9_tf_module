#Instance Module

Module to quickly create three instances.

#How to use
~~~
module "my_ec2_instance" {
  source = "https://gitlab.com/lynzc77/cloud9_tf_module.git"

  ec2_instance_type     = "t3.micro"
  ec2_name              = "my_instance"
  #number_of_instances  = 1
  ec2_ami_id            = < your AMI ID to be used to launch instance"
}
~~~

edit