resource "aws_instance" "sample" {
  ami           = var.ec2_ami_id
  instance_type = var.ec2_instance_type
  count             = var.instance_count

  tags = {
    Name = var.ec2_name
    Name = "Terraform-${count.index + 1}"
    
  }
}
#additional note
#resource "aws_s3_bucket" "example" {
 # bucket = var.bucket_name
#
#}
